# README #

### Common ways to execute this playbook

**confirm what hostnames will be communicated with**

	ansible-playbook -i inventory.ini centos.yml --list-hosts

**confirm what task names will be executed**

	ansible-playbook -i production centos.yml --list-tasks

**confirm what task names will be executed and limit by tag name**

	ansible-playbook -i production centos.yml --list-tasks --tags repo

**'dry run' the execution**

	ansible-playbook -i inventory.ini centos.yml --check

**Execute all tasks on all servers**

	ansible-playbook -i inventory.ini centos.yml

**Execute all tasks for role 'repo' using role scoped tag on all servers**

	ansible-playbook -i inventory.ini centos.yml --tags repo
	
**Execute single task using task scoped tag 'repo-centos-base' on all servers**

	ansible-playbook -i inventory.ini centos.yml --tags repo-centos-base

**Execute 'repo,plugin' tasks only on some inventory group**

	ansible-playbook -i inventory.ini centos.yml --tags "repo,plugin" --limit demo-centos01